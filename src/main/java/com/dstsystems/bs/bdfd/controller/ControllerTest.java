package com.dstsystems.bs.bdfd.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping(value="/test")
public class ControllerTest {

	@RequestMapping(value="hello")
	@ResponseBody
	public String hello() {
		return "<html><body>hello</body></html>";
	}
	
}
